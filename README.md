# Linux Post Installation



## Description
This project is used to hold my customize bash script which I initially to host scripts that I can use to install all applications that I need every time I installed new Linux distribution on my laptop/pc. 

This post installation script can be used for Ubuntu & Arch based distro at the moment. Download post installation script that match your linux distribution.

#### **IMPORTANT**
Before executing the script, give execute permission to the script file by running following command line:
` chmod u+x <script name> `

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Initial Contributor:
- Mark Billy A. Salendu (mark.salendu@gmail.com)
