#!/bin/bash

sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

sudo dnf update

sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel
sudo dnf install lame\* --exclude=lame-devel
sudo dnf group upgrade --with-optional Multimedia

sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo dnf config-manager --add-repo https://packages.microsoft.com/yumrepos/edge

sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'

sudo tee /etc/yum.repos.d/msteams.repo << EOF
[msteams]
name=Microsoft Teams
baseurl=https://packages.microsoft.com/yumrepos/ms-teams
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
EOF

sudo dnf update --refresh

sudo dnf remove -y kmines kmahjongg kpatience elisa-player dragon kmail ktefnet korganizer kaddressbook kpat kolourpaint spectacle
sudo dnf install -y mpv microsoft-edge-stable gimp flameshot code kitty

wget -o ~/.config/kitty/dracula.conf https://gitlab.com/mark.salendu/linux-post-installation/-/raw/main/config/kitty/dracula.conf
wget -o ~/.config/kitty/kitty.conf https://gitlab.com/mark.salendu/linux-post-installation/-/raw/main/config/kitty/kitty.conf
curl -sS https://starship.rs/install.sh | sh





