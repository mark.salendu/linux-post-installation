#!/bin/bash
sudo apt remove -y sticky xed hexchat thunderbird drawing pix xfce4-sreenshooter cheese gnome-2048 gnome-mahjongg rhythmbox aisleriot gnome-sudoku gnome-mines sgt-puzzles hexchat xfce4-notes
sudo apt autoremove --purge

sudo apt install -y software-properties-common apt-transport-https wget
wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"

curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-dev.list'
sudo rm microsoft.gpg

sudo apt-add-repository ppa:remmina-ppa-team/remmina-next
sudo add-apt-repository multiverse
sudo add-apt-repository ppa:gerardpuig/ppa
sudo add-apt-repository ppa:appimagelauncher-team/stable
sudo apt-get update 

sudo apt -y install mousepad gimp gthumb mpv plank flameshot code microsoft-edge-stable remmina remmina-plugin-rdp remmina-plugin-secret flatpak gnome-software-plugin-flatpak guvcview gufw ubuntu-cleaner kitty software-properties-common appimagelauncher gnome-boxes curl

wget –O teams.deb https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams/teams_1.3.00.5153_amd64.deb
sudo apt install ./teams*.deb -y
sudo apt install ttf-mscorefonts-installer -y
sudo apt upgrade -y

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
wget -o ~/.config/kitty/dracula.conf https://gitlab.com/mark.salendu/linux-post-installation/-/raw/main/config/kitty/dracula.conf
wget -o ~/.config/kitty/kitty.conf https://gitlab.com/mark.salendu/linux-post-installation/-/raw/main/config/kitty/kitty.conf
curl -sS https://starship.rs/install.sh | sh
