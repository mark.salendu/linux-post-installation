#!/bin/bash
sudo pacman -Sy mousepad gimp gthumb mpv flameshot code flatpak --noconfirm
sudo pacman -Rsnc geany leafpad pidgin hexchat thunderbird --noconfirm
yay -S microsoft-edge-stable-bin

sudo pacman -Syuu --noconfirm

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

sudo pacman -S pacman-contrib
