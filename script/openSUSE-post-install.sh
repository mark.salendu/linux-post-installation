#!/bin/bash

sudo zypper ar https://packages.microsoft.com/yumrepos/edge edge
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo zypper ar https://packages.microsoft.com/yumrepos/ms-teams/ ms-teams
sudo zypper addrepo https://packages.microsoft.com/yumrepos/vscode vscode
sudo zypper refresh
sudo zypper update -y

sudo zypper remove -y spectacle
sudo zypper install -y mpv gimp flameshot kitty microsoft-edge-stable teams code flatpak git opi

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak update -y

wget -o ~/.config/kitty/dracula.conf https://gitlab.com/mark.salendu/linux-post-installation/-/raw/main/config/kitty/dracula.conf
wget -o ~/.config/kitty/kitty.conf https://gitlab.com/mark.salendu/linux-post-installation/-/raw/main/config/kitty/kitty.conf

